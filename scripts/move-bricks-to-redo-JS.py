import os, sys, glob
from astropy.table import Table, Column
import numpy as n
from astropy.coordinates import SkyCoord
os.chdir('/global/homes/c/comparat/LSDR10_software/reports')
import shutil

redo = Table.read("bricks-dr10-redo.fits")
Abr = Table.read("survey-bricks-dr10Flags.fits")

isin_jc = n.isin( redo['brickname'], Abr['BRICKNAME'][Abr['is_dr10_slice_JC']] )
isin_js = n.isin( redo['brickname'], Abr['BRICKNAME'][Abr['is_dr10_slice_JS']] )
isin_mf = n.isin( redo['brickname'], Abr['BRICKNAME'][Abr['is_dr10_slice_MF']] )

#redo['brickname'][isin_jc]
dr10_js = '/global/cscratch1/sd/jsnigula/dr10c'

def mv_file(p2f, out_loc, dr10_dir, out_dir):
    p2_out = os.path.join( out_dir, out_loc)
    #print(p2f, out_loc, dr10_dir, out_dir, p2_out)
    print('mkdir -p '+p2_out)
    os.system('mkdir -p '+p2_out)
    print('mv '+ os.path.join( dr10_dir, p2f) +' '+ p2_out)
    os.system('mv '+ os.path.join( dr10_dir, p2f) +' '+ p2_out)

#
# for JS
#
out_dir_js = os.path.join( dr10_js, 'TO-REMOVE')
os.mkdir(out_dir_js)
brick_names = redo['brickname'][isin_js]


for brick_name in brick_names:
    mv_file( os.path.join( 'checkpoints', brick_name[:3], 'checkpoint-'+brick_name+'.pickle'), os.path.join( 'checkpoints', brick_name[:3] ) , dr10_js, out_dir_js )
    mv_file( os.path.join( 'logs'       , brick_name[:3],  brick_name+'.log')                , os.path.join( 'logs'       , brick_name[:3] ) , dr10_js, out_dir_js )
    mv_file( os.path.join( 'tractor'    , brick_name[:3], 'tractor-'+brick_name+'.fits')     , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_js, out_dir_js )
    mv_file( os.path.join( 'tractor'    , brick_name[:3], 'brick-'+brick_name+'.sha256sum')  , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_js, out_dir_js )
    mv_file( os.path.join( 'tractor-i'  , brick_name[:3], 'tractor-i-'+brick_name+'.fits')   , os.path.join( 'tractor-i'  , brick_name[:3] ) , dr10_js, out_dir_js )
    mv_file( os.path.join( 'coadd'  , brick_name[:3], brick_name)   , os.path.join( 'coadd'  , brick_name[:3] ) , dr10_js, out_dir_js )
    #
    p2_metric = os.path.join( 'metrics', brick_name[:3])
    mv_file( os.path.join( p2_metric, 'all-models-'+brick_name+'.fits'         ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'blobs-'+brick_name+'.fits.gz'           ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'outlier-mask-'+brick_name+'.fits.fz'    ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'outliers-masked-neg-'+brick_name+'.jpg' ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'outliers-masked-pos-'+brick_name+'.jpg' ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'outliers-post-'+brick_name+'.jpg'       ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'outliers-pre-'+brick_name+'.jpg'        ), p2_metric , dr10_js, out_dir_js )
    mv_file( os.path.join( p2_metric, 'reference-'+brick_name+'.fits'          ), p2_metric , dr10_js, out_dir_js )
    #
    p2_pickles = os.path.join( 'pickles', brick_name[:3])
    mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-srcs.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-tims.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-coadds.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-fitblobs.pickle'         ), p2_pickles , dr10_js, out_dir_js)
