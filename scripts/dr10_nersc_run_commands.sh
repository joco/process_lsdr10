# login to NERSC
mkdir ~/LSDR10_software
cd ~/LSDR10_software
# anaconda install, by default will be in your $HOME/anaconda3/
wget https://repo.anaconda.com/archive/Anaconda3-2021.11-Linux-x86_64.sh
bash Anaconda3-2020.02-Linux-x86_64.sh

# remove existing environment (if already there), optional, for a re-install
# conda env remove -n sdo-dr10

# creates the environment
conda create -n sdo-dr10 python=3.9
# activate the environment
conda activate sdo-dr10
# install dependencies
python3 -m pip install psycopg2-binary
python3 -m pip install jinja2 bokeh 
# I added these dependencies for my own convenience ( I like to debug in ipython and I find astropy.Table quick and easy to use), the following commands is not strictly necessary
python3 -m pip install astropy ipython
# clone the inofficial qdo version
git clone https://bitbucket.org/dstn/qdo.git
cd qdo
# install it
python3 -m pip install .
# now sdo
cd ..
git clone https://github.com/ziyaointl/sdo.git
cd sdo
# in this directory, you will need to  
# * add a list of bricks to process
# for example 
vim dr10-test.txt
# paste :
0702m590
0706m590
0711m590
0716m590
0703m587
0708m587
0712m587
0717m587
0702m585
0707m585
0711m585
0716m585
0701m582
0706m582
0711m582
0715m582
# ESC :wq ENTER

# Then edit:
# - stage_instances.py
# - settings.py
# - templates/runbrick/runbrick-shifter.sh
# before editing too much, let's save a copy :
cp settings.py settings.save.py
cp stage_instances.py stage_instances.save.py
cp templates/runbrick/runbrick-shifter.sh templates/runbrick/runbrick-shifter.save.sh

## settings.py ##

# CFS DR10 directory, to read the data
$LEGACY_SURVEY_DIR
/global/project/projectdirs/cosmo/work/legacysurvey/dr10-rocfs

# DR10 directory to write the outputs
$OUTDIR
/global/cscratch1/sd/dstn/dr10c

# SDO directory
$SDO_DIR
/global/homes/c/comparat/LSDR10_software/sdo
# replace '/global/homes/c/comparat' by your own $HOME

# reporting directory
$PUBLIC_REPORT_PATH
/global/homes/c/comparat/LSDR10_software/reports
# replace '/global/homes/c/comparat' by your own $HOME

## stage_instances.py ##

# name of the qdo queue
PREFIX = 'jc-dr10-south-'
# change the settings per node, number of hours, queue, ...

# Finally run :
python3 init.py
#"Creating queue jc-dr10-south-0 with owner comparat"
python3 main.py

# If everything works, start the infinite loop to re-submit jobs until the list is finished :
# while true; do python -u main.py | tee -a logs.txt; sleep 10m; done

### useful commands ###
-----------------------
### QDO
# list qdo queues
qdo list

# cancel a complete qdo queue 
# qdo delete jc-dr10-south-0 --force

# consult the status
qdo status jc-dr10-south-0
qdo tasks jc-dr10-south-0 --state=Running
qdo tasks jc-dr10-south-0 --state=Failed

qdo tasks jocoDR10-1 > tasks_jocoDR10-1.txt
qdo tasks jocoDR10-2 > tasks_jocoDR10-2.txt
qdo tasks jocoDR10-3 > tasks_jocoDR10-3.txt

qdo tasks jcDR10-L > jun17_tasks_jcDR10-L.txt

### SLURM
# list slurm queue
squeue -u comparat
# cancel all slurm job of a user
# scancel -u comparat

### directories ###

ls /global/cscratch1/sd/dstn/dr10c
ls /global/cscratch1/sd/dstn/dr10c/tractor

less /global/cscratch1/sd/dstn/dr10c/logs/070/0702m590.log

tail -n 1  /global/cscratch1/sd/dstn/dr10c/logs/070/0702m590.log

###
### to re-generate the brick list
###
import os, sys, glob
from astropy.table import Table, Column
import numpy as n

tractor_arr = n.array( glob.glob( "/global/cscratch1/sd/comparat/dr10/tractor/???/*.fits" ) )
bricks_w_tractor = n.array([os.path.basename(el)[8:-5] for el in tractor_arr])

brick_list_Q3 = n.loadtxt('resa-20pc.txt', dtype = 'str')
is_done = n.isin(brick_list_Q3, bricks_w_tractor)
failed = brick_list_Q3[(is_done==False)]
t = Table()
t['BRICKNAME'] = failed
t.write('../reports/bricks-not_successful-Q3.ascii', format='ascii', overwrite = True)


log_arr = n.array( glob.glob( "/global/cscratch1/sd/dstn/dr10c/logs/???/*.log" ) )
log_brick = n.array([os.path.basename(el)[:-4] for el in log_arr])
DATA = []
notFinished = []
for ee, bb in zip(log_arr, log_brick) :
    f1 = open(ee, 'r')
    last_line = f1.readlines()[-1]
    if last_line[:8] == 'All done':
        DATA.append( n.hstack(( bb, last_line.split() )) )
    else:
        print(bb, last_line)
        notFinished.append(bb)
DATA = n.transpose(DATA)


t0 = Table()
t0['bricks'] = n.array( n.hstack((notFinished)) )
t0.write('efeds_not_finished.ascii', format='ascii', overwrite = True)

t1 = Table(DATA.T)
t1.write('brick_list_finished_metadata.ascii', format='ascii', overwrite = True)

t2 = Table()
t2['bricks']=DATA[0]
t2.write('bricks_list_finished.ascii', format='ascii', overwrite = True)

print(len(n.isin(t2['bricks'], bricks_w_tractor).nonzero()[0]))
print(len(n.isin(bricks_w_tractor, t2['bricks']).nonzero()[0]))

# remove from brick  list the ones that are done :
full_brick_list = n.loadtxt('sdo/efeds.txt', unpack = True, dtype='str')
to_process = n.isin(full_brick_list, bricks_w_tractor, invert = True)

t3 = Table()
t3['bricks']=full_brick_list[to_process]
t3.write('efeds_1.txt', format='ascii', overwrite = True)

###
###
###
import numpy as n

f = open('jocoDR10-3.html', 'r')
lines = f.readlines()
mainline = lines[25]
#  mainline[1:100]
data = mainline[35:-5]

sss = data.split('{')

for jj, el in enumerate(sss):
    print(jj, len(el))

sss[145][:10]

p1 = sss[145].split('[')
psuccess = p1[-1].split(']')[0]
suc_list = psuccess.split(',')
has_success = (n.array(suc_list) == '"Succeeded"')
failed_bricks = (has_success == False)

bnames = n.array( (p1[1].split(']')[0]).split(',') )
bname = n.array([ el.strip('"') for el in bname ])

from astropy.table import Table, Column
t = Table()
t['BRICKNAME'] = bname
t.write('bricks-not_successful.ascii', format='ascii')

# import pandas as pd
# dfs = pd.read_html('jocoDR10-3.html')
# df = dfs[0]  # pd.read_html reads in all tables and returns a list of DataFrames

from bs4 import BeautifulSoup
soup = BeautifulSoup(f)#, 'lxml')

from collections import OrderedDict

d = OrderedDict()
for th in zip(soup.select('{')):
    d[th.text.strip()] = td.text.strip().splitlines()

pprint(d)

/global/cscratch1/sd/dstn/dr10c/metrics/070/outliers-post-0702m590.jpg

Logging to: /global/cscratch1/sd/dstn/dr10c//logs/070/0706m590.log
All done: 52240.713 s worker CPU, 52405.153 s worker Wall, Wall: 7409.910 s, Cores in use: 7.20, Total efficiency (on 8 cores): 90.0 %, VmPeak: 17597 MB, VmSize: 7378 MB, VmRSS: 6463 MB, VmData: 6502 MB, maxrss: 16.159488 MB

Logging to: /global/cscratch1/sd/dstn/dr10c//logs/070/0702m590.log
All done: 61465.181 s worker CPU, 61619.663 s worker Wall, Wall: 14225.422 s, Cores in use: 4.45, Total efficiency (on 8 cores): 55.6 %, VmPeak: 18942 MB, VmSize: 7469 MB, VmRSS: 6304 MB, VmData: 6342 MB, maxrss: 17.222108 MB


Logging to: /global/cscratch1/sd/dstn/dr10c//logs/071/0716m590.log
All done: 54910.476 s worker CPU, 54928.065 s worker Wall, Wall: 7574.184 s, Cores in use: 7.40, Total efficiency (on 8 cores): 92.4 %, VmPeak: 20161 MB, VmSize: 8498 MB, VmRSS: 7919 MB, VmData: 7959 MB, maxrss: 18.922652 MB

Logging to: /global/cscratch1/sd/dstn/dr10c//logs/071/0711m590.log
All done: 41302.325 s worker CPU, 41318.025 s worker Wall, Wall: 6103.080 s, Cores in use: 6.94, Total efficiency (on 8 cores): 86.7 %, VmPeak: 19808 MB, VmSize: 8247 MB, VmRSS: 7667 MB, VmData: 7707 MB, maxrss: 18.70216 MB



need_brick_per_day = 200_000/(4*7)
need_cpuS_per_day = need_brick_per_day * 50_000
need_cpu = need_cpuS_per_day / ( 24 * 3600 )
need_nodes = need_cpu/32


Logging to: /global/cscratch1/sd/dstn/dr10c/logs/143/1434p060.log
Logging to: /global/cscratch1/sd/dstn/dr10c/logs/143/1437p060.log
Logging to: /global/cscratch1/sd/dstn/dr10c/logs/143/1439p060.log
Logging to: /global/cscratch1/sd/dstn/dr10c/logs/143/1432p060.log





cd /global/cscratch1/sd/comparat/dr10

ls /global/cscratch1/sd/comparat/dr10/tractor/0??/tractor-????????.fits > jc0.list
ls /global/cscratch1/sd/comparat/dr10/tractor/1??/tractor-????????.fits > jc1.list
ls /global/cscratch1/sd/comparat/dr10/tractor/2??/tractor-????????.fits > jc2.list
ls /global/cscratch1/sd/comparat/dr10/tractor/3??/tractor-????????.fits > jc3.list

wc -l jc*.list
  23887 jc0.list
  42945 jc1.list
  33222 jc2.list
  20472 jc3.list
 120526 total
 120993 total
 121142 total


ls /global/cscratch1/sd/jsnigula/dr10c/tractor/0??/tractor-????????.fits > js0.list
ls /global/cscratch1/sd/jsnigula/dr10c/tractor/1??/tractor-????????.fits > js1.list
ls /global/cscratch1/sd/jsnigula/dr10c/tractor/2??/tractor-????????.fits > js2.list
ls /global/cscratch1/sd/jsnigula/dr10c/tractor/3??/tractor-????????.fits > js3.list

wc -l js*.list

22017 js0.list
 5005 js1.list
10878 js2.list
15884 js3.list
53784 total
66480 total


ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/0??/tractor-????????.fits > mf0.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/1??/tractor-????????.fits > mf1.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/2??/tractor-????????.fits > mf2.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/3??/tractor-????????.fits > mf3.list

wc -l mf*.list

13458 mf0.list
10335 mf1.list
 9520 mf2.list
 8853 mf3.list
42166 total
56956 total

cat js*.list > js_full_processed.list
cat mf*.list > mf_full_processed.list

cat mf*.list jc*.list js*.list > full_processed.list



ls /global/cscratch1/sd/comparat/dr10/pickles/0??/runbrick-????????-srcs.pickle > jc0.pickleslist
ls /global/cscratch1/sd/comparat/dr10/pickles/1??/runbrick-????????-srcs.pickle > jc1.pickleslist
ls /global/cscratch1/sd/comparat/dr10/pickles/2??/runbrick-????????-srcs.pickle > jc2.pickleslist
ls /global/cscratch1/sd/comparat/dr10/pickles/3??/runbrick-????????-srcs.pickle > jc3.pickleslist

wc -l jc?.pickleslist

173 jc0.pickleslist
391 jc1.pickleslist
501 jc2.pickleslist
    91 jc3.pickleslist
1156 total
  402 total



ls /global/cscratch1/sd/jsnigula/dr10c/pickles/0??/runbrick-????????-srcs.pickle > js0.pickleslist
ls /global/cscratch1/sd/jsnigula/dr10c/pickles/1??/runbrick-????????-srcs.pickle > js1.pickleslist
ls /global/cscratch1/sd/jsnigula/dr10c/pickles/2??/runbrick-????????-srcs.pickle > js2.pickleslist
ls /global/cscratch1/sd/jsnigula/dr10c/pickles/3??/runbrick-????????-srcs.pickle > js3.pickleslist

wc -l js*.pickleslist


  1401 js0.pickleslist
   184 js1.pickleslist
   823 js2.pickleslist
  1224 js3.pickleslist
  3632 total
  4403 total

ls /global/cscratch1/sd/mxhf/dr10c-test/pickles/0??/runbrick-????????-srcs.pickle > mf0.pickleslist
ls /global/cscratch1/sd/mxhf/dr10c-test/pickles/1??/runbrick-????????-srcs.pickle > mf1.pickleslist
ls /global/cscratch1/sd/mxhf/dr10c-test/pickles/2??/runbrick-????????-srcs.pickle > mf2.pickleslist
ls /global/cscratch1/sd/mxhf/dr10c-test/pickles/3??/runbrick-????????-srcs.pickle > mf3.pickleslist

wc -l mf*.pickleslist
   726 mf0.pickleslist
   340 mf1.pickleslist
   379 mf2.pickleslist
   356 mf3.pickleslist
  1801 total
  3869 total


rsync -azv comparat@cori.nersc.gov:/global/cscratch1/sd/comparat/dr10/tractor/??? .
rsync -azv comparat@cori.nersc.gov:/global/cscratch1/sd/jsnigula/dr10c/tractor/??? .
rsync -azv comparat@cori.nersc.gov:/global/cscratch1/sd/mxhf/dr10c-test/tractor/??? .



cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/coadd
find > ../../find_to_remove_coadd.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/checkpoints
find > ../../find_to_remove_checkpoints.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/coadd
find > ../../find_to_remove_coadd.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/logs
find > ../../find_to_remove_logs.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/metrics
find > ../../find_to_remove_metrics.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/pickles
find > ../../find_to_remove_pickles.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/tractor
find > ../../find_to_remove_tractor.txt

cd /global/cscratch1/sd/comparat/dr10/TO-REMOVE/tractor-i
find > ../../find_to_remove_tractor-i.txt



cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/coadd
find > ../../find_to_rsync_coadd.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/checkpoints
find > ../../find_to_rsync_checkpoints.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/coadd
find > ../../find_to_rsync_coadd.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/logs
find > ../../find_to_rsync_logs.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/metrics
find > ../../find_to_rsync_metrics.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/pickles
find > ../../find_to_rsync_pickles.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/tractor
find > ../../find_to_rsync_tractor.txt

cd /global/cscratch1/sd/comparat/dr10/TO-RSYNC/tractor-i
find > ../../find_to_rsync_tractor-i.txt




cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/coadd
find > ../../find_to_remove_coadd.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/checkpoints
find > ../../find_to_remove_checkpoints.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/coadd
find > ../../find_to_remove_coadd.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/logs
find > ../../find_to_remove_logs.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/metrics
find > ../../find_to_remove_metrics.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/pickles
find > ../../find_to_remove_pickles.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/tractor
find > ../../find_to_remove_tractor.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-REMOVE/tractor-i
find > ../../find_to_remove_tractor-i.txt





cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/checkpoints
find > ../../find_to_rsync_checkpoints.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/logs
find > ../../find_to_rsync_logs.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/metrics
find > ../../find_to_rsync_metrics.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/pickles
find > ../../find_to_rsync_pickles.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/tractor
find > ../../find_to_rsync_tractor.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/tractor-i
find > ../../find_to_rsync_tractor-i.txt

cd /global/cscratch1/sd/mxhf/dr10c-test/TO-RSYNC/coadd
find > ../../find_to_rsync_coadd.txt

cd /global/cscratch1/sd/comparat/dr10
chmod -R  o+rX TO-RSYNC
