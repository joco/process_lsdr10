"""
cd /global/homes/c/comparat/LSDR10_software/reports

qdo tasks jocoDR10-1 > tasks_jocoDR10-1.txt
qdo tasks jocoDR10-2 > tasks_jocoDR10-2.txt
qdo tasks jocoDR10-3 > tasks_jocoDR10-3.txt

qdo tasks jsDR10-high-0                       > tasks_jsDR10-high-0.txt
qdo tasks mf-dr10-south-50pc-0                > tasks_mf-dr10-south-50pc-0.txt
qdo tasks jsDR10-low-0                        > tasks_jsDR10-low-0.txt
qdo tasks mf-dr10-south-50pc-highmem          > tasks_mf-dr10-south-50pc-highmem.txt
qdo tasks mf-dr10-south-50pc-highmem_cleanv4  > tasks_mf-dr10-south-50pc-highmem_cleanv4.txt


cd /global/cscratch1/sd/comparat/dr10

ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/0??/tractor-????????.fits > mf0.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/1??/tractor-????????.fits > mf1.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/2??/tractor-????????.fits > mf2.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/3??/tractor-????????.fits > mf3.list

ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/0??/tractor-????????.fits > mf0.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/1??/tractor-????????.fits > mf1.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/2??/tractor-????????.fits > mf2.list
ls /global/cscratch1/sd/mxhf/dr10c-test/tractor/3??/tractor-????????.fits > mf3.list

cat js*.list > js_full_processed.list
cat mf*.list > mf_full_processed.list

cp js_full_processed.list /global/homes/c/comparat/LSDR10_software/reports
cp mf_full_processed.list /global/homes/c/comparat/LSDR10_software/reports

"""

import os, sys, glob
from astropy.table import Table, Column
import numpy as n
from astropy.coordinates import SkyCoord
os.chdir('/global/homes/c/comparat/LSDR10_software/reports')
import shutil

redo = Table.read("bricks-dr10-redo.fits")
Abr = Table.read("survey-bricks-dr10Flags.fits")
Q1 = n.loadtxt('/global/homes/c/comparat/LSDR10_software/sdo/jun17_tasks_jcDR10-L.txt', unpack = True, dtype='str')

#Q1 = n.loadtxt('tasks_jocoDR10-1.txt', unpack = True, dtype='str')
#Q2 = n.loadtxt('tasks_jocoDR10-2.txt', unpack = True, dtype='str')
#Q3 = n.loadtxt('tasks_jocoDR10-3.txt', unpack = True, dtype='str')

Q1_br = Q1[1][Q1[0]=="Succeeded"]
#Q2_br = Q2[1][Q2[0]=="Succeeded"]
#Q3_br = Q3[1][Q3[0]=="Succeeded"]

dr10_jc = '/global/cscratch1/sd/comparat/dr10'
#dr10_js = '/global/cscratch1/sd/jsnigula/dr10c'
#dr10_mf = '/global/cscratch1/sd/mxhf/dr10c-test'

#JSQ1 = n.loadtxt('js_full_processed.list', unpack = True, dtype='str')
#MFQ1 = n.loadtxt('mf_full_processed.list', unpack = True, dtype='str')

#JS_B = n.array([os.path.basename(el)[8:-5] for el in JSQ1 ])
#MF_B = n.array([os.path.basename(el)[8:-5] for el in MFQ1 ])

# write the list of bricks to be computed
# JC
#succedeed_bricks = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JC']], n.hstack((Q1_br, Q2_br, Q3_br)) )
#succedeed_bricks = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JC']], Q1_br )
#isin_jc = n.isin( redo['brickname'], Abr['BRICKNAME'][Abr['is_dr10_slice_JC']] )

#to_compute_1 = Abr['BRICKNAME'][Abr['is_dr10_slice_JC']][succedeed_bricks==False]
#to_compute_2 = redo['brickname'][isin_jc]

#all_bricks = n.unique( n.hstack(( to_compute_1.value, to_compute_2.value )) )
#high_mem = n.isin(all_bricks, to_compute_1)

## Writing the bricks
#t = Table()
#t['BRICKNAME'] = all_bricks[high_mem]
#t.write('/global/u1/c/comparat/LSDR10_software/sdo/dr10_slice_JC_highMEM.txt',overwrite=True, format='ascii')

#t = Table()
#t['BRICKNAME'] = all_bricks[high_mem==False]
#t.write('/global/u1/c/comparat/LSDR10_software/sdo/dr10_slice_JC_lowMEM.txt',overwrite=True, format='ascii')

##
## write the list of bricks to be computed
## JS
##
##
#succedeed_bricks_js = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JS']], JS_B )
#to_redo_bricks_js = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JS']], redo['brickname'] )
##
#to_compute_js = Abr['BRICKNAME'][Abr['is_dr10_slice_JS']][(succedeed_bricks_js==False)|(to_redo_bricks_js)]
#all_bricks_js = n.unique( to_compute_js.value )
## Writing the bricks
#t = Table()
#t['BRICKNAME'] = all_bricks_js
#t.write('/global/u1/c/comparat/LSDR10_software/sdo/dr10_slice_JS_REMAINING.txt',overwrite=True, format='ascii')

##
## write the list of bricks to be computed
## MF
##
##
#succedeed_bricks_mf = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_MF']], MF_B )
#to_redo_bricks_mf = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_MF']], redo['brickname'] )
##
#to_compute_mf = Abr['BRICKNAME'][Abr['is_dr10_slice_MF']][(succedeed_bricks_mf==False)|(to_redo_bricks_mf)]
#all_bricks_mf = n.unique( to_compute_mf.value )
## Writing the bricks
#t = Table()
#t['BRICKNAME'] = all_bricks_mf
#t.write('/global/u1/c/comparat/LSDR10_software/sdo/dr10_slice_MF_REMAINING.txt',overwrite=True, format='ascii')



def mv_file(p2f, out_loc, dr10_dir, out_dir):
    p2_out = os.path.join( out_dir, out_loc)
    #print(p2f, out_loc, dr10_dir, out_dir, p2_out)
    print('mkdir -p '+p2_out)
    os.system('mkdir -p '+p2_out)
    print('mv '+ os.path.join( dr10_dir, p2f) +' '+ p2_out)
    os.system('mv '+ os.path.join( dr10_dir, p2f) +' '+ p2_out)

#bricks to rsync (finished ones) :
#sel_to_rsync = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JC']], all_bricks, invert = True )
#sel_to_redo = n.isin( Abr['BRICKNAME'][Abr['is_dr10_slice_JC']], redo['brickname'] )

dr10_jc = '/global/cscratch1/sd/comparat/dr10'
out_dir_jc = os.path.join( dr10_jc, 'TO-RSYNC')

brick_names = Q1_br #Abr['BRICKNAME'][Abr['is_dr10_slice_JC']][(sel_to_rsync)&(sel_to_redo==False)]

for brick_name in brick_names:
    p2_metric = os.path.join( 'metrics', brick_name[:3])
    if os.path.isfile( os.path.join(out_dir_jc, p2_metric, 'reference-'+brick_name+'.fits' ))==False:
        mv_file( os.path.join( 'checkpoints', brick_name[:3], 'checkpoint-'+brick_name+'.pickle'), os.path.join( 'checkpoints', brick_name[:3] ), dr10_jc, out_dir_jc  )
        mv_file( os.path.join( 'logs'       , brick_name[:3],  brick_name+'.log')                , os.path.join( 'logs'       , brick_name[:3] ), dr10_jc, out_dir_jc  )
        mv_file( os.path.join( 'tractor'    , brick_name[:3], 'tractor-'+brick_name+'.fits')     , os.path.join( 'tractor'    , brick_name[:3] ), dr10_jc, out_dir_jc  )
        mv_file( os.path.join( 'tractor'    , brick_name[:3], 'brick-'+brick_name+'.sha256sum')  , os.path.join( 'tractor'    , brick_name[:3] ), dr10_jc, out_dir_jc  )
        mv_file( os.path.join( 'tractor-i'  , brick_name[:3], 'tractor-i-'+brick_name+'.fits')   , os.path.join( 'tractor-i'  , brick_name[:3] ), dr10_jc, out_dir_jc  )
        mv_file( os.path.join( 'coadd'  , brick_name[:3], brick_name)   , os.path.join( 'coadd'  , brick_name[:3] ), dr10_jc, out_dir_jc   )
        #
        mv_file( os.path.join( p2_metric, 'all-models-'+brick_name+'.fits'         ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'blobs-'+brick_name+'.fits.gz'           ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'outlier-mask-'+brick_name+'.fits.fz'    ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'outliers-masked-neg-'+brick_name+'.jpg' ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'outliers-masked-pos-'+brick_name+'.jpg' ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'outliers-post-'+brick_name+'.jpg'       ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'outliers-pre-'+brick_name+'.jpg'        ), p2_metric, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_metric, 'reference-'+brick_name+'.fits'          ), p2_metric, dr10_jc, out_dir_jc )
        #
        p2_pickles = os.path.join( 'pickles', brick_name[:3])
        mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-srcs.pickle'         ), p2_pickles, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-tims.pickle'         ), p2_pickles, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-coadds.pickle'         ), p2_pickles, dr10_jc, out_dir_jc )
        mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-fitblobs.pickle'         ), p2_pickles, dr10_jc, out_dir_jc )

##
## for JS
##

#dr10_js = '/global/cscratch1/sd/jsnigula/dr10c'
#to_rsync_js = n.isin( JS_B, redo['brickname'], invert = True )
#out_dir_js = os.path.join( dr10_js, 'TO-RSYNC')
#os.mkdir(out_dir_js)
#brick_names = JS_B[to_rsync_js]


#for brick_name in brick_names:
    #mv_file( os.path.join( 'checkpoints', brick_name[:3], 'checkpoint-'+brick_name+'.pickle'), os.path.join( 'checkpoints', brick_name[:3] ) , dr10_js, out_dir_js )
    #mv_file( os.path.join( 'logs'       , brick_name[:3],  brick_name+'.log')                , os.path.join( 'logs'       , brick_name[:3] ) , dr10_js, out_dir_js )
    #mv_file( os.path.join( 'tractor'    , brick_name[:3], 'tractor-'+brick_name+'.fits')     , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_js, out_dir_js )
    #mv_file( os.path.join( 'tractor'    , brick_name[:3], 'brick-'+brick_name+'.sha256sum')  , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_js, out_dir_js )
    #mv_file( os.path.join( 'tractor-i'  , brick_name[:3], 'tractor-i-'+brick_name+'.fits')   , os.path.join( 'tractor-i'  , brick_name[:3] ) , dr10_js, out_dir_js )
    #mv_file( os.path.join( 'coadd'  , brick_name[:3], brick_name)   , os.path.join( 'coadd'  , brick_name[:3] ) , dr10_js, out_dir_js )
    ##
    #p2_metric = os.path.join( 'metrics', brick_name[:3])
    #mv_file( os.path.join( p2_metric, 'all-models-'+brick_name+'.fits'         ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'blobs-'+brick_name+'.fits.gz'           ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'outlier-mask-'+brick_name+'.fits.fz'    ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'outliers-masked-neg-'+brick_name+'.jpg' ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'outliers-masked-pos-'+brick_name+'.jpg' ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'outliers-post-'+brick_name+'.jpg'       ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'outliers-pre-'+brick_name+'.jpg'        ), p2_metric , dr10_js, out_dir_js )
    #mv_file( os.path.join( p2_metric, 'reference-'+brick_name+'.fits'          ), p2_metric , dr10_js, out_dir_js )
    ##
    #p2_pickles = os.path.join( 'pickles', brick_name[:3])
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-srcs.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-tims.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-coadds.pickle'         ), p2_pickles , dr10_js, out_dir_js)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-fitblobs.pickle'         ), p2_pickles , dr10_js, out_dir_js)


##
## for MF
##

#dr10_mf = '/global/cscratch1/sd/mxhf/dr10c-test'
#to_rsync_mf = n.isin( MF_B, redo['brickname'], invert = True )
#out_dir_mf = os.path.join( dr10_mf, 'TO-RSYNC')
#os.mkdir(out_dir_mf)
#brick_names = MF_B[to_rsync_mf]

#for brick_name in brick_names:
    #mv_file( os.path.join( 'checkpoints', brick_name[:3], 'checkpoint-'+brick_name+'.pickle'), os.path.join( 'checkpoints', brick_name[:3] ) , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( 'logs'       , brick_name[:3],  brick_name+'.log')                , os.path.join( 'logs'       , brick_name[:3] ) , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( 'tractor'    , brick_name[:3], 'tractor-'+brick_name+'.fits')     , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( 'tractor'    , brick_name[:3], 'brick-'+brick_name+'.sha256sum')  , os.path.join( 'tractor'    , brick_name[:3] ) , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( 'tractor-i'  , brick_name[:3], 'tractor-i-'+brick_name+'.fits')   , os.path.join( 'tractor-i'  , brick_name[:3] ) , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( 'coadd'  , brick_name[:3], brick_name)   , os.path.join( 'coadd'  , brick_name[:3] ) , dr10_mf, out_dir_mf )
    ##
    #p2_metric = os.path.join( 'metrics', brick_name[:3])
    #mv_file( os.path.join( p2_metric, 'all-models-'+brick_name+'.fits'         ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'blobs-'+brick_name+'.fits.gz'           ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'outlier-mask-'+brick_name+'.fits.fz'    ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'outliers-masked-neg-'+brick_name+'.jpg' ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'outliers-masked-pos-'+brick_name+'.jpg' ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'outliers-post-'+brick_name+'.jpg'       ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'outliers-pre-'+brick_name+'.jpg'        ), p2_metric , dr10_mf, out_dir_mf )
    #mv_file( os.path.join( p2_metric, 'reference-'+brick_name+'.fits'          ), p2_metric , dr10_mf, out_dir_mf )
    ##
    #p2_pickles = os.path.join( 'pickles', brick_name[:3])
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-srcs.pickle'         ), p2_pickles , dr10_mf, out_dir_mf)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-tims.pickle'         ), p2_pickles , dr10_mf, out_dir_mf)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-coadds.pickle'         ), p2_pickles , dr10_mf, out_dir_mf)
    #mv_file( os.path.join( p2_pickles, 'runbrick-'+brick_name+'-fitblobs.pickle'         ), p2_pickles , dr10_mf, out_dir_mf)

