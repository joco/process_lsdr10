
main()

starts with running the init()

init()
 - starts the DB of jobs: 'sdo.db'
 - creates a table 'jobs' (if not already existing), commits and closes connection to DB
 - creates a directory to write the script(s) : 'scripts'
 - reads the template to run a single brick : 'runbrick/runbrick-shifter.sh' (meta script used to generate all scripts to be executed)
	- Calls util.read_template 
		- Calls util.make_template_format_friendly
			- Compiles a regular expression pattern r'\$({[^0-9]+?})' into a regular expression object
			- finds all occurences of the pattern in the template to locate the variables {?} and be able to assign them later
		- return 'runbrick_script' =  a python usable template (with variables)
 - for each stage_instance in stage_instances.py
	- call write_runbrick to write the script(s) (one per stage_instance) in the script directory
		- 'runbrick_script' is assigned the variable : .format(LEGACY_SURVEY_DIR, ncores, stage, TELESCOPE, mem, EXTRA_PARAMS, write_stage, OUTDIR)
		- the script is then written.
		- chmod +x -v scripts/*

At the end of the init() we have a set of scripts: script/*.sh to be run.

Then back in the main : 
 - reads the stage_instances (from stage_instances import stage_instances)
 - add tasks to the queue
	- utils.py: QdoCentricStage(Stage).add_tasks()
	- Inherits from Class Stage which is just a container recording the name and a few variables, no methods.
	- QdoCentricStage is the class where all methods are
	
 - schedule the jobs
 - prints the status
 - render reports
