import os, sys, glob
from astropy.table import Table, Column
import numpy as n
from astropy.coordinates import SkyCoord
bricks = Table.read("/home/comparat/data/legacysurvey/dr9/survey-bricks.fits.gz")
ra = bricks['RA']
dec = bricks['DEC']

dr10list = n.loadtxt("/home/comparat/data/nersc/LSDR10_software/sdo/dr10-bricks-omit-lmc-smc.txt", unpack = True, dtype='str' )

is_dr10 = n.isin(bricks['BRICKNAME'], dr10list)

coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
bb_gal = coords.galactic.b.value
ll_gal = coords.galactic.l.value

bb_ecl = coords.barycentrictrueecliptic.lat
ll_ecl = coords.barycentrictrueecliptic.lon

is_erosita = (ll_gal>180) & (is_dr10)

nl = lambda selection : len(selection.nonzero()[0])

print(nl(is_dr10))
print(nl(is_erosita))
print(nl((is_dr10)&(dec>3)))
print(nl((is_dr10)&(dec>=-24)&(dec<=3)))
print(nl((is_dr10)&(dec<-24)))

print(nl((is_erosita)&(is_dr10)&(dec>3)))
print(nl((is_erosita)&(is_dr10)&(dec>=-24)&(dec<=3)))
print(nl((is_erosita)&(is_dr10)&(dec<-24)))

bricks['is_dr10'] = is_dr10
bricks['is_dr10_erosita'] = is_erosita
bricks['is_dr10_slice_JC'] = (is_dr10)&(dec>3)
bricks['is_dr10_slice_MF'] = (is_dr10)&(dec>=-24)&(dec<=3)
bricks['is_dr10_slice_JS'] = (is_dr10)&(dec<-24)

bricks.write('/home/comparat/data/legacysurvey/dr10/survey-bricks-dr10Flags.fits', overwrite = True)

t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_JC']]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/dr10_slice_JC.txt',overwrite=True, format='ascii')

t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_MF']]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/dr10_slice_MF.txt',overwrite=True, format='ascii')

t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_JS']]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/dr10_slice_JS.txt',overwrite=True, format='ascii')

for ii in n.arange(3, 35,1):
    print(ii, nl((bricks['is_dr10_slice_JC']&(dec>ii))) )

print(int( nl(bricks['is_dr10_slice_JC'])*0.01*3.5  ) ) # 5k
print(int( nl(bricks['is_dr10_slice_JC'])*0.025*3.5 ) ) # 12k
print(int( nl(bricks['is_dr10_slice_JC'])*0.2*3.5   ) ) # 100k

print( int( nl(bricks['is_dr10_slice_JC'])*0.01*3.5  ) + int( nl(bricks['is_dr10_slice_JC'])*0.025*3.5 ) + int( nl(bricks['is_dr10_slice_JC'])*0.2*3.5   )     )
print( nl(bricks['is_dr10_slice_JC']) )

print('100k', nl((bricks['is_dr10_slice_JC']&(dec>9.3))) )
print('20k', nl((bricks['is_dr10_slice_JC']&(dec<=9.3)&(dec>4.9))) )
print('8k', nl((bricks['is_dr10_slice_JC']&(dec<=4.9))) )


t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_JC']&(dec<=4.9)]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/resa-1pc.txt',overwrite=True, format='ascii')


t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_JC']&(dec<=9.3)&(dec>4.9)]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/resa-2pc.txt',overwrite=True, format='ascii')


t = Table()
t['BRICKNAME'] = bricks['BRICKNAME'][bricks['is_dr10_slice_JC']&(dec>9.3)]
t.write('/home/comparat/data/nersc/LSDR10_software/sdo/resa-20pc.txt',overwrite=True, format='ascii')

