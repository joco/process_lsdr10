# login to NERSC
mkdir ~/LSDR10_software
cd ~/LSDR10_software
# anaconda install, by default will be in your $HOME/anaconda3/
wget https://repo.anaconda.com/archive/Anaconda3-2021.11-Linux-x86_64.sh
bash Anaconda3-2021.11-Linux-x86_64.sh

# remove existing environment (if already there), optional, for a re-install
# conda env remove -n sdo-dr10

# creates the environment
conda create -n sdo-dr10 python=3.9
# activate the environment
conda activate sdo-dr10
# install dependencies
python3 -m pip install psycopg2-binary
python3 -m pip install jinja2 bokeh 
# I added these dependencies for my own convenience ( I like to debug in ipython and I find astropy.Table quick and easy to use), the following commands is not strictly necessary
python3 -m pip install astropy ipython
# clone the inofficial qdo version
git clone https://bitbucket.org/dstn/qdo.git
cd qdo
git checkout ssh-srun
# OR more directly
# git clone -b ssh-srun https://bitbucket.org/dstn/qdo.git

# install it
python3 -m pip install .
# now sdo
cd ..
git clone https://github.com/ziyaointl/sdo.git
cd sdo
# in this directory, you will need to  
# * add a list of bricks to process
# for example 
vim dr10-test.txt
# i, paste :
0702m590
0706m590
0711m590
0716m590
0703m587
0708m587
0712m587
0717m587
0702m585
0707m585
0711m585
0716m585
0701m582
0706m582
0711m582
0715m582
# ESC :wq ENTER

# Then edit:
# - stage_instances.py
# - settings.py
# - templates/runbrick/runbrick-shifter.sh
# before editing too much, let's save a copy :
cp settings.py settings.save.py
cp stage_instances.py stage_instances.save.py
cp templates/runbrick/runbrick-shifter.sh templates/runbrick/runbrick-shifter.save.sh

## settings.py ##

# CFS DR10 directory, to read the data
$LEGACY_SURVEY_DIR
/global/project/projectdirs/cosmo/work/legacysurvey/dr10-rocfs

# DR10 directory to write the outputs
$OUTDIR
    /global/cscratch1/sd/comparat/dr10

# SDO directory
$SDO_DIR
/global/homes/c/comparat/LSDR10_software/sdo
# replace '/global/homes/c/comparat' by your own $HOME

# reporting directory
$PUBLIC_REPORT_PATH
/global/homes/c/comparat/LSDR10_software/reports
# replace '/global/homes/c/comparat' by your own $HOME

# latest image :
docker:legacysurvey/legacypipe:DR10.0.0

## stage_instances.py ##

# name of the qdo queue
PREFIX = 'jc-dr10-south-'
# change the settings per node, number of hours, queue, ...
# list of bricks :
task_srcs=[FileTaskSource("dr10-test.txt")],

## templates/runbrick/runbrick-shifter.sh ##
# need to change GAIA dir to this one :
export GAIA_CAT_DIR=$COSMO/data/gaia/edr3/healpix
# and release number to this one :
--release 10000 \

stage_instances = gen_stages([
    StageDefinition(
        prev_stage=None,
        tasks_per_nodehr=4,
        cores_per_worker=8,
        cores_per_worker_actual=8,
        mem_per_worker=HASWELL_MEM // 4,
        arch='haswell',
        # 1% run
        name=PREFIX + '0',
        qname=PREFIX + '0',
        qos=debug(), # 1% reservation. 210 nodes 10 hours
        task_srcs=[FileTaskSource("efeds-10.txt")],
        job_duration=0.49,
        max_nodes_per_job=1,
        max_number_of_jobs=1,
        allocation='m3592',
        stage='writecat',
        write_stage=['srcs', 'tims'],
        revive_all=False,)
    ,
    StageDefinition(
        prev_stage=None,
        tasks_per_nodehr=4,
        cores_per_worker=8,
        cores_per_worker_actual=8,
        mem_per_worker=HASWELL_MEM // 4,
        arch='haswell',
        # 1% run
        name=PREFIX + '1',
        qname=PREFIX + '1',
        qos=reservation('dr10_1pc'), # 1% reservation. 210 nodes 10 hours
        task_srcs=[FileTaskSource("resa-1pc.txt")],
        job_duration=9.999,
        max_nodes_per_job=30,
        max_number_of_jobs=21,
        allocation='m3592',
        stage='writecat',
        write_stage=['srcs', 'tims'],
        revive_all=False,)
    ,
     StageDefinition(
        prev_stage=None,
        tasks_per_nodehr=4,
        cores_per_worker=8,
        cores_per_worker_actual=8,
        mem_per_worker=HASWELL_MEM // 4,
        arch='haswell',
        #qos=regular(),
        #qos=debug(),
        # 2.5% run
        name=PREFIX + '2',
        qname=PREFIX + '2',
        qos=reservation('dr10run'), # 2.5% reservation. 210 nodes 24 hours
        task_srcs=[FileTaskSource("resa-2pc.txt")],
        job_duration=23.999,
        max_nodes_per_job=35,
        max_number_of_jobs=18,
        allocation='m3592',
        stage='writecat',
        write_stage=['srcs', 'tims'],
        revive_all=False,)
    ,
     StageDefinition(
        prev_stage=None,
        tasks_per_nodehr=4,
        cores_per_worker=8,
        cores_per_worker_actual=8,
        mem_per_worker=HASWELL_MEM // 4,
        arch='haswell',
        # 20%
        name=PREFIX + '3',
        qname=PREFIX + '3',
        qos=reservation('dr10_20pc'), # 20% reservation. 240 nodes 168 hours
        task_srcs=[FileTaskSource("resa-20pc.txt")],
        job_duration=23.999,
        max_nodes_per_job=60,
        max_number_of_jobs=84,
        allocation='m3592',
        stage='writecat',
        write_stage=['srcs', 'tims'],
        revive_all=False,)
    ]
    )


# Add in your ~/.bashrc the following 
# PATH export
export QDO_DB_HOST=db-loadbalancer.cosmo-qdo-db.production.svc.spin.nersc.org
export QDO_DB_NAME=qdo
export QDO_DB_PASS=2e86d3f09c4e17d8d84e
export QDO_DB_USER=qdo
export QDO_BACKEND=postgres
export SCR='/global/cscratch1/sd/dstn'

# might need to add in your ~/.bashrc the following 
export PATH="/global/homes/c/comparat/anaconda3/envs/sdo-dr10/bin:$PATH"

# Add in your ~/.profile the following 
# to make written files world-readable 
umask 0003
# umask -S

!conda

# Finally run :
python3 init.py
#"Creating queue jc-dr10-south-0 with owner comparat"
python3 main.py

# If everything works, start the infinite loop to re-submit jobs until the list is finished :
while true; do python -u main.py | tee -a logs.txt; sleep 10m; done




### useful commands ###
-----------------------
### QDO
# list qdo queues
qdo list

# cancel a complete qdo queue 
# qdo delete jc-dr10-south-0 --force
qdo delete jocoDR10-0 --force
# qdo delete jocoDR10-1 --force
# qdo delete jocoDR10-2 --force
# qdo delete jocoDR10-3 --force

# consult the status
qdo status jc-dr10-south-0
qdo tasks jc-dr10-south-0 --state=Running
qdo tasks jc-dr10-south-0 --state=Failed


### SLURM
# list slurm queue
squeue -u comparat
# cancel all slurm job of a user
# scancel -u comparat

### directories ###
/global/project/projectdirs/cosmo/work/legacysurvey/dr10-rocfs
/global/cscratch1/sd/dstn/dr10c
/global/cscratch1/sd/dstn/dr10c/tractor

# changing permissions, if needed
cd /global/cscratch1/sd/dstn/dr10c
chmod o+rX -R both
chmod o+rX -R checkpoints
chmod o+rX -R coadd
chmod o+rX -R haswell
chmod o+rX -R logs
chmod o+rX -R metrics
chmod o+rX -R pickles
chmod o+rX -R tractor
chmod o+rX -R tractor-i
